import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1>Hola mundo {{name}} - Prueba de Angular5</h1>`,
})
export class AppComponent  { name = 'Angular 7'; }
